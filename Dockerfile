FROM java:openjdk-8-alpine
WORKDIR /
COPY ./build/libs/* ./build/libs/demo.jar
EXPOSE $PORT
CMD java -Dserver.port=$PORT -jar build/libs/demo.jar