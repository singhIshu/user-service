package com.icecream.demo.service

import com.icecream.demo.exception.TeamNotFoundException
import com.icecream.demo.model.User
import com.icecream.demo.repository.UserRepository
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.util.Optional

@RunWith(MockitoJUnitRunner::class)
class UserServiceTest {

    private val userRepository: UserRepository = mock(UserRepository::class.java)

    private lateinit var userService: UserService

    @Before
    @Throws(Exception::class)
    fun setUp() {
        userService = UserService(userRepository)
    }

    @Test
    fun shouldAddTeam() {
        val magneto = User("magneto@gmail.com","Magneto", "123")
        userService.addUser(magneto.email, magneto.name, magneto.password)
        verify(userRepository, times(1)).save(ArgumentMatchers.any(User::class.java))
    }

    @Test
    fun shouldDeleteTeam() {
        val email = "magneto@gmail.com"
        userService.deleteTeam(email)
        verify(userRepository, times(1)).deleteByEmail(email)
    }

    @Test(expected = TeamNotFoundException::class)
    @Throws(TeamNotFoundException::class)
    fun shouldThrowExceptionIfEmailIdNotFound() {
        val email = "abcd@gmail.com"
        `when`(userRepository.findByEmail(email)).thenReturn(Optional.empty())
        userService.getTeamByEmail(email)
         verify(userService, times(1)).getTeamByEmail(email);
    }

    @Test
    @Throws(TeamNotFoundException::class)
    fun shouldGetTheTeamInfoIfEmailIdIsValid() {
        val email = "abcd@gmail.com"
        val magneto = User("Magneto", "magneto@gmail.com", "abcd")
        `when`(userRepository.findByEmail(email)).thenReturn(Optional.of(magneto))
        userService.getTeamByEmail(email)
        verify(userRepository, times(1)).findByEmail(email)
    }

    @Test
    fun shouldGetAllTheTeams() {
        userService.findAllTeams()
        verify(userRepository, times(1)).findAll()
    }
}
