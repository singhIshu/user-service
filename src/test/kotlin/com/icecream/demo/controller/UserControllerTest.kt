package com.icecream.demo.controller

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

internal class UserControllerTest {

    lateinit var userController: UserController

    @Before
    internal fun setUp() {
        userController = UserController()
    }

    @Test
    fun sayHello() {
        assertEquals(userController.sayHello(),"Hello User")
    }
}