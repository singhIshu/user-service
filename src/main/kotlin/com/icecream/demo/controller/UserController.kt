package com.icecream.demo.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController {

    @GetMapping("/")
    fun sayHello (): String {
        return "Hello I am User service"
    }
}