package com.icecream.demo.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "User")
data class User(
        @Id val email: String,
        val name: String,
        val password: String)