package com.icecream.demo.repository

import com.icecream.demo.model.User
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
interface UserRepository : MongoRepository<User, String> {
    fun deleteByEmail(email: String)
    fun findByEmail(email: String): Optional<User>
    fun existsByPassword(password: String): Boolean
}
