package com.icecream.demo.service

import com.icecream.demo.exception.TeamNotFoundException
import com.icecream.demo.model.User
import com.icecream.demo.repository.UserRepository
import org.mindrot.jbcrypt.BCrypt
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class UserService @Autowired
constructor(private val userRepository: UserRepository) {

    val allUsers: List<User>
        get() = userRepository.findAll()

    fun addUser(email: String, name: String, password: String) {
        val encryptedPassword = BCrypt.hashpw(password, BCrypt.gensalt())
        val user = User(email,name, encryptedPassword)
        userRepository.save(user)
    }

    fun deleteTeam(email: String) {
        userRepository.deleteByEmail(email)
    }

    fun getTeamByEmail(email: String): User {
        val team = userRepository.findByEmail(email)
        return if (team.isPresent) {
            team.get()
        } else {
            throw TeamNotFoundException("User with this email is not present")
        }
    }

    fun findAllTeams(): List<User> {
        return userRepository.findAll()
    }

    fun isValidEmailAndPassword(email: String, password: String): Boolean {
        val team = userRepository.findByEmail(email)
        return team.filter { user -> BCrypt.checkpw(password, user.password) }.isPresent
    }

    fun getName(teamID: String): String {
        return userRepository.findById(teamID).get().name
    }
}
