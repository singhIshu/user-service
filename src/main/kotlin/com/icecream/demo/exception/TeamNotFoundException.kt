package com.icecream.demo.exception

import java.lang.Exception

class TeamNotFoundException(message: String): Exception(message)
