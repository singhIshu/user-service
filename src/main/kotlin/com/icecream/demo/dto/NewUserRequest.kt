package com.icecream.demo.dto

data class NewUserRequest(val name: String, val email: String, val password: String)
